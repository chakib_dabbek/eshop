<?php

namespace BoutiqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/back_produit")
 */
class ProduitController extends Controller
{
    /**
     * @Route(path="/", name="back_list_produits")
     */
    public function indexAction()
    {
        return $this->render('BoutiqueBundle:produits:listProduits.html.twig');
    }
}
