<?php

namespace BoutiqueBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use WebBundle\Entity\Boutique;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
class DefaultController extends Controller
{

    /**
     * @Route("/", name="boutique_backend")
     */
    public function indexAction()
    {
        return $this->render('BoutiqueBundle:dashbord:dashbord.html.twig');
    }
    public function create_boutiqueAction()
    {
        //$secteurs= $this->getDoctrine()->getRepository('WebBundle:SecteurActivity')->findAll();

        return $this->render('BoutiqueBundle::boutique.html.twig');
    }
    /**
     * Creates a new boutique entity.
     *
     * @Route("/new", name="new_boutique")
     * @Method({"GET", "POST"})
     *
     */
    public function newboutiqueAction(Request $request){
        $boutique =new Boutique();
      //  dump('aaaa');die();
        $form = $this->createForm('BoutiqueBundle\Form\BoutiqueType', $boutique)
            ->add('saveAndCreateNew', 'Symfony\Component\Form\Extension\Core\Type\SubmitType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


        //    $file = $product->getImage();

//            if ($file != NULL) {
//
//                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
//
//                $file->move(
//                    $this->getParameter('image_directory'), $fileName
//                );
//
//                $product->setImage($fileName);
//            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($boutique);
            $entityManager->flush();

            // See http://symfony.com/doc/current/book/controller.html#flash-messages
            $this->addFlash('success', 'Le boutique crée avec succes');

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('boutique_backend');
            }

            return $this->redirectToRoute('boutique_backend');
        }

        return $this->render('@Boutique/boutique.html.twig', array(
            'post' => $boutique,
            'form' => $form->createView(),
        ));
    }

}
