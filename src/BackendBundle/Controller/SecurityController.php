<?php

namespace BackendBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

/**
 * {@inheritDoc}
 */
class SecurityController extends BaseController
{

    /**
     * {@inheritDoc}
     */
    public function renderLogin(array $data)
    {
        $requestAttributes = $this->container->get('request')->attributes;
        $authChecker = $this->container->get('security.authorization_checker');

        if ('admin_login' === $requestAttributes->get('_route')) {
            if ($authChecker->isGranted('ROLE_SUPER_ADMIN')||$authChecker->isGranted('ROLE_GESTIONNAIRE')||$authChecker->isGranted('ROLE_GROSSISTE')) {
               $template = sprintf('BackendBundle:Default:index.html.twig');
              
              
            } else {
                $template = sprintf('BackendBundle:Security:login.html.twig');
            }
        } else {
            $template = sprintf('FOSUserBundle:Security:login.html.twig');
        }

        return $this->container->get('templating')->renderResponse($template, $data);
    }

}
