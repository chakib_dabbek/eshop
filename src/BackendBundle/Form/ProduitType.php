<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Vich\UploaderBundle\Form\Type\VichFileType;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class ProduitType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // see http://symfony.com/doc/current/reference/forms/types.html
        // $builder->add('title', null, array('required' => false, ...));

        $builder
                ->add('nom', null, array(
                    'attr' => array('autofocus' => true),
                    'label' => 'Nom',
                ))
                ->add('description', 'Symfony\Component\Form\Extension\Core\Type\TextareaType', array('label' => 'Description'))
                ->add('prix', 'Symfony\Component\Form\Extension\Core\Type\MoneyType', array(
                    'currency' => false,
                    'divisor' => 1000,
                ))
                ->add('nb_achat', 'Symfony\Component\Form\Extension\Core\Type\TextType', array())
                ->add('quantite', 'Symfony\Component\Form\Extension\Core\Type\TextType', array())

                ->add('is_active', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array(
                    'label' => 'Est Active?',
                    'required' => false,
                    'data' => true,
                ))
                ->add('categorie', EntityType::class, array(
                    'class' => 'WebBundle\Entity\Categories',
                    'choice_label' => 'getNom',
                ))
                ->add('image', FileType::class, array('label' => 'Image', 'required' => FALSE, 'data_class' => NULL))
             //   ->add('image1', FileType::class, array('label' => 'Image1', 'required' => FALSE, 'data_class' => NULL))
            //    ->add('image2', FileType::class, array('label' => 'Image2', 'required' => FALSE, 'data_class' => NULL))
            //    ->add('image3', FileType::class, array('label' => 'Image3', 'required' => FALSE, 'data_class' => NULL))

            ->add('file', VichFileType::class, array(
                    'required' => false,
                    'allow_delete' => true, // not mandatory, default is true
                    'download_link' => true, // not mandatory, default is true
                ))

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'WebBundle\Entity\Produits',
        ));
    }

}
