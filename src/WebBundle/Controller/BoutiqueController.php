<?php

namespace WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\Boutique;
use WebBundle\Form\BoutiqueType;

/**
 * Boutique controller.
 *
 * @Route("backend/boutique")
 */
class BoutiqueController extends Controller
{
    /**
     * Lists all Boutique entities.
     *
     * @Route("/", name="boutique_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $boutiques = $em->getRepository('WebBundle:Boutique')->findAll();

        return $this->render('boutique/index.html.twig', array(
            'boutiques' => $boutiques,
        ));
    }

    /**
     * Creates a new Boutique entity.
     *
     * @Route("/new", name="boutique_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $boutique = new Boutique();
        $form = $this->createForm('WebBundle\Form\BoutiqueType', $boutique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($boutique);
            $em->flush();

            return $this->redirectToRoute('boutique_show', array('id' => $boutique->getId()));
        }

        return $this->render('boutique/new.html.twig', array(
            'boutique' => $boutique,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Boutique entity.
     *
     * @Route("/{id}", name="boutique_show")
     * @Method("GET")
     */
    public function showAction(Boutique $boutique)
    {
        $deleteForm = $this->createDeleteForm($boutique);

        return $this->render('boutique/show.html.twig', array(
            'boutique' => $boutique,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Boutique entity.
     *
     * @Route("/{id}/edit", name="boutique_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Boutique $boutique)
    {
        $deleteForm = $this->createDeleteForm($boutique);
        $editForm = $this->createForm('WebBundle\Form\BoutiqueType', $boutique);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($boutique);
            $em->flush();

            return $this->redirectToRoute('boutique_edit', array('id' => $boutique->getId()));
        }

        return $this->render('boutique/edit.html.twig', array(
            'boutique' => $boutique,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Boutique entity.
     *
     * @Route("/{id}", name="boutique_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Boutique $boutique)
    {
        $form = $this->createDeleteForm($boutique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($boutique);
            $em->flush();
        }

        return $this->redirectToRoute('boutique_index');
    }

    /**
     * Creates a form to delete a Boutique entity.
     *
     * @param Boutique $boutique The Boutique entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Boutique $boutique)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('boutique_delete', array('id' => $boutique->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
