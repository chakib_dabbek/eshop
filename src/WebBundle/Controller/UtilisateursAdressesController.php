<?php

namespace WebBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use WebBundle\Entity\UtilisateursAdresses;
use WebBundle\Form\UtilisateursAdressesType;

/**
 * UtilisateursAdresses controller.
 *
 * @Route("/backend/useraddress")
 */
class UtilisateursAdressesController extends Controller
{
    /**
     * Lists all UtilisateursAdresses entities.
     *
     * @Route("/", name="backend_useraddress_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $utilisateursAdresses = $em->getRepository('WebBundle:UtilisateursAdresses')->findAll();

        return $this->render('utilisateursadresses/index.html.twig', array(
            'utilisateursAdresses' => $utilisateursAdresses,
        ));
    }

    /**
     * Creates a new UtilisateursAdresses entity.
     *
     * @Route("/new", name="backend_useraddress_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $utilisateursAdress = new UtilisateursAdresses();
        $form = $this->createForm('WebBundle\Form\UtilisateursAdressesType', $utilisateursAdress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($utilisateursAdress);
            $em->flush();

            return $this->redirectToRoute('backend_useraddress_show', array('id' => $utilisateursAdress->getId()));
        }

        return $this->render('utilisateursadresses/new.html.twig', array(
            'utilisateursAdress' => $utilisateursAdress,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a UtilisateursAdresses entity.
     *
     * @Route("/{id}", name="backend_useraddress_show")
     * @Method("GET")
     */
    public function showAction(UtilisateursAdresses $utilisateursAdress)
    {
        $deleteForm = $this->createDeleteForm($utilisateursAdress);

        return $this->render('utilisateursadresses/show.html.twig', array(
            'utilisateursAdress' => $utilisateursAdress,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing UtilisateursAdresses entity.
     *
     * @Route("/{id}/edit", name="backend_useraddress_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, UtilisateursAdresses $utilisateursAdress)
    {
        $deleteForm = $this->createDeleteForm($utilisateursAdress);
        $editForm = $this->createForm('WebBundle\Form\UtilisateursAdressesType', $utilisateursAdress);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($utilisateursAdress);
            $em->flush();

            return $this->redirectToRoute('backend_useraddress_edit', array('id' => $utilisateursAdress->getId()));
        }

        return $this->render('utilisateursadresses/edit.html.twig', array(
            'utilisateursAdress' => $utilisateursAdress,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a UtilisateursAdresses entity.
     *
     * @Route("/{id}", name="backend_useraddress_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, UtilisateursAdresses $utilisateursAdress)
    {
        $form = $this->createDeleteForm($utilisateursAdress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($utilisateursAdress);
            $em->flush();
        }

        return $this->redirectToRoute('backend_useraddress_index');
    }

    /**
     * Creates a form to delete a UtilisateursAdresses entity.
     *
     * @param UtilisateursAdresses $utilisateursAdress The UtilisateursAdresses entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UtilisateursAdresses $utilisateursAdress)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('backend_useraddress_delete', array('id' => $utilisateursAdress->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
