<?php

namespace WebBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Mgilet\NotificationBundle\Annotation\Notifiable;
use Mgilet\NotificationBundle\NotifiableInterface;

class BoutiqueType extends AbstractType implements NotifiableInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('raisonsociale')
            ->add('nomdomaine')
            ->add('email')
            ->add('description')
            ->add('tel')
            ->add('couleur')
            ->add('logo')
            ->add('secteur')
            ->add('responsable')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WebBundle\Entity\Boutique'
        ));
    }
}
